<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 30);
            $table->string('apeidoP', 30);
            $table->string('apeidoM', 30);
            $table->string('matricula', 15);
            $table->string('grupo', 5);
            $table->string('grado', 15);
            $table->string('promedio', 5)->nullable();
            $table->string('correo', 50);
            $table->string('contrasenia', 255);
            $table -> string('ine', 255)->nullable();
            $table -> string('acta',255)->nullable();
            $table -> string('curp',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
