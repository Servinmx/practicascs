@extends('layout.main-alumno')
@section('titulo', "Calificaciones")
@section('css')
@endsection
@section('contenido')
    <div class="container">
        <div class="row course_boxes">
            <div class="col-lg-12" id="wrapper">
                <div class="d-flex flex-column" id="content-wrapper">
                    <div id="content">
                        <div class="container-fluid">
                            <h3 class="text-dark mb-4">Calificaciones</h3>
                            <div class="card shadow">
                                <div class="card-header py-3">
                                    <p class="text-primary m-0 font-weight-bold">Informacion tus calificaciones</p>
                                </div>
                                <div class="card-body">

                                    <div class="table-responsive table mt-2" id="tabla" role="grid" aria-describedby="dataTable_info">
                                        <table class="table my-0" id="dataTabla">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Materia</th>
                                                <th>Clave</th>
                                                <th>Calificación</th>
                                            </tr>
                                            </thead>
                                            <tbody id="calificaciones">

                                            @foreach($calificaciones as $calificacion)
                                            <tr>
                                                @foreach($materias as $materia)
                                                    @if ($materia->id == $calificacion -> id_materia)
                                                        <td>{{$materia->id}}</td>
                                                        <td>{{$materia->nombre}}</td>
                                                        <td>{{$materia->clave}}</td>
                                                    @endif
                                                @endforeach
                                                <td>{{$calificacion -> calificacion}}</td>
                                            </tr>
                                            @endforeach
                                            <tfoot>
                                            <tr>
                                                <td><strong>#</strong></td>
                                                <td><strong>Materia</strong></td>
                                                <td><strong>Clave</strong></td>
                                                <td><strong>Calificación</strong></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <i aria-hidden="true"></i>
                                        <label for="promedio">Promedio</label>
                                        <input readonly type="text" class="form-control form-control-user" id="promedio" name="promedio"  value="{{$promedio}}">
                                    </div>
                                    <a class="btn btn-outline-primary" href="{{route('pdf')}}">Descargar PDF</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function (){
            $('#dataTabla').DataTable();
        });
    </script>
@endsection



