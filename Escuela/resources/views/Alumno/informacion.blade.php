@extends('layout.main-alumno')
@section('titulo', "Informacion")
@section('css')
@endsection
@section('contenido')
    <div class="container">
        <div class="row course_boxes">
            <div class="col-lg-12" id="wrapper">
                <div class="d-flex flex-column" id="content-wrapper">
                    <div id="content">
                        <div class="container-fluid">
                            <h3 class="text-dark mb-4">Información</h3>
                            <div class="card shadow">
                                <div class="card-header py-3">
                                    <p class="text-primary m-0 font-weight-bold">Tu información</p>
                                </div>
                                <div class="card-body">
                                    <form method="post"  action="{{route('editarAlumno')}}" enctype="multipart/form-data">
                                        <div class="card-body">
                                            {{ csrf_field() }}
                                            <input class="form-control" type="text" id="id" name="id"  value="{{session('alumno')->id}}" hidden>
                                            <div class="form-group row">
                                                <div class="col-sm-12 mb-6 mb-sm-0">
                                                    <i aria-hidden="true"></i>
                                                    <label for="nombre">Nombre</label>
                                                    <input type="text" class="form-control form-control-user" id="nombre" name="nombre" value="{{session('alumno')->nombre}}"required >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <i aria-hidden="true"></i>
                                                    <label for="apeidoP">Apeido Paterno</label>
                                                    <input type="text" class="form-control form-control-user" id="apeidoP" name="apeidoP" value="{{session('alumno')->apeidoP}}" required>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <i aria-hidden="true"></i>
                                                    <label for="apeidoM">Apeido Materno</label>
                                                    <input type="text" class="form-control form-control-user" id="apeidoM" name="apeidoM" value="{{session('alumno')->apeidoM}}"required >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <i aria-hidden="true"></i>
                                                    <label for="matricula">Matricula</label>
                                                    <input type="text" class="form-control form-control-user" id="matricula" name="matricula" value="{{session('alumno')->matricula}}"required >
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <i aria-hidden="true"></i>
                                                    <label for="grado">Grado</label>
                                                    <input type="text" class="form-control form-control-user" id="grado" name="grado" value="{{session('alumno')->grado}}"required >
                                                </div>

                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <i aria-hidden="true"></i>
                                                    <label for="grupo">Grupo</label>
                                                    <input type="text" class="form-control form-control-user" id="grupo" name="grupo" value="{{session('alumno')->grupo}}" required >
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <i aria-hidden="true"></i>
                                                    <label for="promedio">Promedio</label>
                                                    <input readonly type="text" class="form-control form-control-user" id="promedio" name="promedio"  value="{{session('alumno')->promedio}}">
                                                </div>

                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <i aria-hidden="true"></i>
                                                    <label for="correo">Correo</label>
                                                    <input type="email" class="form-control form-control-user" id="correo" name="correo"  value="{{session('alumno')->correo}}" required>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <i aria-hidden="true"></i>
                                                    <label for="contrasenia1"> Contraseña</label>
                                                    <input type="password" class="form-control form-control-user" id="contrasenia1" placeholder="Contraseña" name="contrasenia1" required>
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary btn-user btn-block" id="editar" value="Editar Información">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function ()
        {
            $("#editar").on("click"), (function (){
                var id = $("#id");
                var nombre = $("#nombre");
                var apeidoP = $("#apeidoP");
                var apeidoM = $("#apeidoM");
                var matricula = $("#matricula");
                var grado = $("#grado");
                var grupo = $("#grupo");
                var promedio = $("#promedio");
                var correo = $("#correo");
                var contrasenia1 = $("#contrasenia1");
                var params = {"id":id, "nombre":nombre, "apeidoP":apeidoP, "apeidoM": apeidoM, "matricula":matricula, "grupo":grupo, "grado":grado, "promedio":promedio, "correo":correo, "contrasenia1":contrasenia1}
                $.ajax({
                    method: "post",
                    data: params,
                    url:"/editarAlumno",
                    success: function (response)
                    {
                        swal("Editado");
                    }
                })
            });
        });
    </script>
@endsection
