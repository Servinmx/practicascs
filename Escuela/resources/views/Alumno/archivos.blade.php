@extends('layout.main-alumno')
@section('titulo', "Archivos")
@section('css')
@endsection
@section('contenido')
    <div class="container">
        <div class="row course_boxes">
            <div class="col-lg-12" id="wrapper">
                <div class="d-flex flex-column" id="content-wrapper">
                    <div id="content">
                        <div class="container-fluid">
                            <h3 class="text-dark mb-4">Archivos</h3>
                            <div class="card shadow">
                                <div class="card-header py-3">
                                    <p class="text-primary m-0 font-weight-bold">Para tener tu informacion actualizada, sube los siguientes archivos en formato pdf/jpg</p>
                                </div>
                                <div class="card-body">
                                    <label class="text-danger">
                                        @if(isset($estatus))
                                            <label class="text-danger">{{$mensaje}}</label>
                                        @endif
                                    </label>
                                    <form class="user" method="post" action="{{route('archivosA')}}" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input class="form-control" type="text" id="id" name="id"  value="{{session('alumno')->id}}" hidden>
                                        <div class="form-group row" >
                                            <div class="col-sm-4 mb-2 mb-sm-0">
                                                <label for="ine">INE</label>
                                                <input type="file" class="form-control form-control-user" id="ine" name="ine" placeholder="INE">
                                            </div>
                                            <div class="col-sm-4 mb-2 mb-sm-0">
                                                <label for="curp">CURP</label>
                                                <input type="file" class="form-control form-control-user" id="curp" name="curp" placeholder="CURP">
                                            </div>
                                            <div class="col-sm-4 mb-2 mb-sm-0">
                                                <label for="acta">Acta de nacimiento</label>
                                                <input type="file" class="form-control form-control-user" id="acta" name="acta" placeholder="Acta de nacimiento">
                                            </div>
                                        </div>
                                        <input type="submit" value="Subir" class="btn btn-primary btn-user btn-block">
                                        <hr>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
