<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Alumno-Registro</title>
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
    <link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="styles/main_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/responsive.css">
    <!-- Custom fonts for this template-->
    <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="/css/sb-admin-2.min.css" rel="stylesheet">
</head>
<body class="bg-gradient-primary" style="background-color: #9ED4F9">
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block bg-register-image" style="background-image: url(images/course_4.jpg)">
                </div>
                <div class="col-lg-7">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">¡Crear Cuenta!</h1>
                        </div>
                        <label class="text-danger">
                            @if(isset($estatus))
                                <label class="text-danger">{{$mensaje}}</label>
                            @endif
                        </label>
                        <form class="form-control" method="post" action="{{route('crearA')}}">
                            {{csrf_field()}}
                            <div class="form-group row">
                                <div class="col-sm-12 mb-6 mb-sm-0">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control form-control-user" id="nombre" name="nombre" placeholder="Nombre">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="apeidoP">Apeido Paterno</label>
                                    <input type="text" class="form-control form-control-user" id="apeidoP" name="apeidoP" placeholder="Apeido Paterno">
                                </div>
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="apeidoM">Apeido Materno</label>
                                    <input type="text" class="form-control form-control-user" id="apeidoM" name="apeidoM" placeholder="Apeido Materno">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4 mb-2 mb-sm-0">
                                    <label for="matricula">Matricula</label>
                                    <input type="text" class="form-control form-control-user" id="matricula" name="matricula" placeholder="Matricula">
                                </div>
                                <div class="col-sm-4 mb-2 mb-sm-0">
                                    <label for="grupo">Grupo</label>
                                    <input type="number" class="form-control form-control-user" id="grupo" name="grupo" placeholder="grupo">
                                </div>
                                <div class="col-sm-4 mb-2 mb-sm-0">
                                    <label for="grado">Grado</label>
                                    <input type="number" class="form-control form-control-user" id="grado" name="grado" placeholder="Grado">
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12 mb-6 mb-sm-0">
                                    <label for="correo">Correo</label>
                                    <input type="email" class="form-control form-control-user" id="correo" name="correo" placeholder="Correo">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="contrasenia1"> Contraseña</label>
                                    <input type="password" class="form-control form-control-user" id="contrasenia1" placeholder="Contraseña" name="contrasenia1">
                                </div>
                                <div class="col-sm-6">
                                    <label for="contrasenia2"> Repetir contraseña</label>
                                    <input type="password" class="form-control form-control-user" id="contrasenia2" placeholder="Repetir contraseña" name="contrasenia2">
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary btn-user btn-block" value="Crear Cuenta">
                            <hr>
                        </form>
                        <hr>
                        <div class="text-center">
                            <a class="small" href="{{route('loginA')}}" >¿Tienes una cuenta? ¡Inicia Sesión!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="/js/sb-admin-2.min.js"></script>

</body>

</html>
