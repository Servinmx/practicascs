@extends('layout.main-docente')
@section('titulo', "Materias")
@section('css')
@endsection
@section('contenido')
    <div class="container">
        <div class="row course_boxes">
            <div class="col-lg-12" id="wrapper">
                <div class="d-flex flex-column" id="content-wrapper">
                    <div id="content">
                        <div class="container-fluid">
                            <h3 class="text-dark mb-4">Nueva materia</h3>
                            <div class="card shadow">
                                <div class="card-body">
                                    <label class="text-danger">
                                        @if(isset($estatus))
                                            <label class="text-danger">{{$mensaje}}</label>
                                        @endif
                                    </label>
                                    <form class="user" method="post" action="{{route('addMateria')}}">
                                        {{csrf_field()}}
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-6 mb-sm-0">
                                                <label for="nombre">Nombre</label>
                                                <input type="text" class="form-control form-control-user" id="nombre" name="nombre" placeholder="Nombre" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label for="clave">Clave</label>
                                                <input type="text" class="form-control form-control-user" id="clave" name="clave" placeholder="Clave" required>
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label for="horas">Horas</label>
                                                <input type="number" class="form-control form-control-user" id="horas" name="horas" placeholder="Horas" required>
                                            </div>
                                        </div>
                                        <input type="submit" class="btn btn-primary btn-user btn-block" value="Añadir materia">
                                    </form>
                                    <hr>
                                    <a class="btn btn-outline-primary" href="{{route('graficas')}}">Ver materias</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
