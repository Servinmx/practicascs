@extends('layout.main-docente')
@section('titulo', "Alumnos")
@section('css')
@endsection
@section('contenido')
    <div class="container">
        <div class="row course_boxes">
            <div class="col-lg-12" id="wrapper">
                <div class="d-flex flex-column" id="content-wrapper">
                    <div id="content">
                        <div class="container-fluid">
                            <h3 class="text-dark mb-4">Calificar</h3>
                            <label class="text-danger">
                                @if(isset($estatus))
                                    <label class="text-danger">{{$mensaje}}</label>
                                @endif
                            </label>
                            <form class="user" method="post"  action="{{route('calificaciones')}}">
                                {{csrf_field()}}
                                <div class="card shadow">
                                    <div class="card-header row">
                                            <div class="col-lg-3">
                                                <h4 class="text-primary m-0 font-weight-bold">Selecciona una materia</h4>
                                            </div>
                                            <div class="col-lg-3">
                                                <select class="form-control" id="materia" name="materia" required>
                                                    @foreach($materias as $materia)
                                                        <option value="{{$materia->id}}">{{$materia->nombre}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    <div class="card-body">
                                            <div class="table-responsive table mt-2" id="tabla" role="grid" aria-describedby="dataTable_info">
                                                <table class="table my-0" id="dataTabla">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nombre</th>
                                                        <th>Matricula</th>
                                                        <th>Calificación</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($alumnos as $alumno)
                                                        <tr>
                                                            <input class="form-control" id="alu_{{$alumno->id}}alumno" name="alu_{{$alumno->id}}alumno"  value="{{$alumno->id}}" type="hidden">
                                                            <td>{{$alumno->id}}</td>
                                                            <td>{{$alumno->nombre}}</td>
                                                            <td>{{$alumno->matricula}}</td>
                                                            <td>
                                                                <input class="form-control" type="number" id="cal_{{$alumno->id}}calificacion" name="cal_{{$alumno->id}}calificacion" required>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td><strong>#</strong></td>
                                                        <td><strong>Nombre</strong></td>
                                                        <td><strong>Matricula</strong></td>
                                                        <td><strong>Calificación</strong></td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    <div class="col-lg-12">
                                            <input type="submit" class="btn btn-primary btn-user btn-block" value="Calificar">
                                        </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function (){
            $('#dataTabla').DataTable();
        });
    </script>

@endsection
