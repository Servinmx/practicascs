@extends('layout.main-docente')
@section('titulo', "Alumnos")
@section('css')
@endsection
@section('contenido')
    <div class="container">
        <div class="row course_boxes">
            <div class="col-lg-12" id="wrapper">
                <div class="d-flex flex-column" id="content-wrapper">
                    <div id="content">
                        <div class="container-fluid">
                            <h3 class="text-dark mb-4">Alumnos</h3>
                            <div class="card shadow">
                                <div class="card-header py-3">
                                    <p class="text-primary m-0 font-weight-bold">Informacion de los alumnos</p>
                                </div>
                                <div class="card-body">
                                    <div class="row" style="margin-top: 1%">
                                    </div>
                                    <div class="table-responsive table mt-2" id="tabla" role="grid" aria-describedby="dataTable_info">
                                        <table class="table my-0" id="dataTabla">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nombre</th>
                                                <th>Grupo</th>
                                                <th>Matricula</th>
                                                <th>Promedio</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($alumnos as $alumno)
                                                <tr>
                                                    <td>{{$alumno->id}}</td>
                                                    <td>{{$alumno->nombre}}</td>
                                                    <td>{{$alumno->grupo}}</td>
                                                    <td>{{$alumno->matricula}}</td>
                                                    <td>{{$alumno->promedio}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td><strong>#</strong></td>
                                                <td><strong>Nombre</strong></td>
                                                <td><strong>Grupo</strong></td>
                                                <td><strong>Matricula</strong></td>
                                                <td><strong>Promedio</strong></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h6>Buscar alumno</h6>
                        <label class="text-danger">
                            @if(isset($estatus))
                                <label class="text-danger">{{$mensaje}}</label>
                            @endif
                        </label>
                        <form method="post" action="{{route('buscarA')}}">
                            {{csrf_field()}}
                            <div class="col-sm-12 mb-3 mb-sm-0" style="margin-top: 10px">
                                <i aria-hidden="true"></i>
                                <input type="text" class="form-control form-control-user" id="nombre" name="nombre" placeholder="Nombre">
                            </div>
                            <div class="col-sm-12 mb-3 mb-sm-0" style="margin-top: 10px">
                                <input type="submit" class="btn btn-primary btn-user btn-block" value="Buscar alumno">
                            </div>
                        </form>

                        <div class="col-lg-6"  align="rigth" style="margin-top: 10px">
                            <a class="btn btn-outline-primary" href="{{route('calificar')}}">Calificar</a>
                        </div>
                    </div>
                </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function (){
            $('#dataTabla').DataTable();
        });
    </script>
@endsection
