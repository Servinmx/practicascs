@extends('layout.main-docente')
@section('titulo', "Alumnos")
@section('css')
@endsection
@section('contenido')
    <div class="container">
        <div class="row course_boxes">
            <div class="col-lg-12" id="wrapper">
                <div class="d-flex flex-column" id="content-wrapper">
                    <div id="content">
                        <div class="container-fluid">
                            <h3 class="text-dark mb-4">Información</h3>
                            <div class="card shadow">
                                <div class="card-header py-3">
                                    <p class="text-primary m-0 font-weight-bold">Tu información</p>
                                </div>
                                <div class="card-body">
                                    <form class="user" method="post"  action="{{route('editarDocente')}}" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input class="form-control" type="text" id="id" name="id"  value="{{session('docente')->id}}" hidden>
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-6 mb-sm-0">
                                                <label for="nombre">Nombre</label>
                                                <input type="text" class="form-control form-control-user" id="nombre" name="nombre" value="{{session('docente')->nombre}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label for="apeidoP">Apeido Paterno</label>
                                                <input type="text" class="form-control form-control-user" id="apeidoP" name="apeidoP" value="{{session('docente')->apeidoP}}" required>
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label for="apeidoM">Apeido Materno</label>
                                                <input type="text" class="form-control form-control-user" id="apeidoM" name="apeidoM" value="{{session('docente')-> apeidoM}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-6 mb-sm-0">
                                                <label for="cedula">Cedula</label>
                                                <input type="number" class="form-control form-control-user" id="cedula" name="cedula" value="{{session('docente')->cecula}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label for="correo">Correo</label>
                                                <input type="email" class="form-control form-control-user" id="correo" name="correo" value="{{session('docente')->correo}}" required>
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label for="contrasenia1"> Contraseña</label>
                                                <input type="password" class="form-control form-control-user" id="contrasenia1" name="contrasenia1" placeholder="Contraseña" required>
                                            </div>
                                        </div>
                                        <input type="submit" class="btn btn-primary btn-user btn-block" value="Editar Información">
                                        <hr>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
