@extends('layout.main-docente')
@section('titulo', "Materias")
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('css')
@endsection
@section('contenido')
    <div class="container">
        <div class="row course_boxes">
            <div class="col-lg-12" id="wrapper">
                <div class="d-flex flex-column" id="content-wrapper">
                    <div id="content">
                        <div class="container-fluid">
                            <h3 class="text-dark mb-4">Materias </h3>
                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="table-responsive table mt-2" id="tabla" role="grid" aria-describedby="dataTable_info">
                                        <table class="table my-0" id="dataTabla">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nombre</th>
                                                <th>Clave</th>
                                                <th>Horas</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($materias as $materia)
                                                <tr>
                                                    <td>{{$materia->id}}</td>
                                                    <td>{{$materia->nombre}}</td>
                                                    <td>{{$materia->clave}}</td>
                                                    <td>{{$materia->horas}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td><strong>#</strong></td>
                                                <td><strong>Nombre</strong></td>
                                                <td><strong>Clave</strong></td>
                                                <td><strong>Horas</strong></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-lg-6"  align="rigth">
                                    <a class="btn btn-outline-primary" href="#">Agregar materia</a>
                                </div>
                                <hr>
                                <canvas id="grafica"></canvas>
                                @foreach($materias as $materia)
                                    <div class="col-lg-12">
                                        <h6>{{$materia->nombre}}</h6>
                                        <canvas id="{{$materia->id}}"></canvas>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script>
        $(document).ready(function (){
            $('#dataTabla').DataTable();
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
   <script>
       var grafica = {{$materia->id}};
       parseInt(grafica);
       console.log(grafica);
       var datos = [];
       var info;
       var materias = [];
       var valores = [];
       var partes = [];
       var nom;
       var cal;
       var cont = 1;
       var cantidad = 1;
       var calificaciones = [];
       var agrupadoValores = [];
       var agrupadoCalificaciones = [];

        for(var x=1; x<= grafica; x++) {
        console.log(x);

        $.ajax({
            url: './generar',
            method: 'get',
            data: {
                id: x,
            }
        }).done(function (response) {
            console.log(response);
            for (var p = 0; p < response.length; p++) {
                info = response[p];
                partes = info.split(";");
                nom = parseInt(partes[0]);
                cal = parseInt(partes[1]);
                materias [p] = nom;
                calificaciones [p] = cal;

            }

            for(q=0; q<= calificaciones.length;q++){
                if((materias[q]==cantidad)){
                    agrupadoValores [q] = materias[q];
                    agrupadoCalificaciones [q] = calificaciones[q];
                }
            }
            cantidad = cantidad +1;
            generar(cont,agrupadoValores,agrupadoCalificaciones);
            cont = cont +1;
        });
        console.log(materias[0]);
        console.log(calificaciones[0]);
        }

       function generar (cont,agrupadoValores,agrupadoCalificaciones){
           var ctx = document.getElementById(cont).getContext('2d');
           var myChart = new Chart(ctx, {
               type: 'bar',
               data: {
                   labels:agrupadoCalificaciones,
                   datasets: [{
                       label: 'Control',
                       data: agrupadoCalificaciones,
                       backgroundColor: [
                           'rgba(255, 99, 132, 0.2)',
                           'rgba(54, 162, 235, 0.2)',
                           'rgba(255, 206, 86, 0.2)',
                           'rgba(75, 192, 192, 0.2)',
                           'rgba(153, 102, 255, 0.2)',
                           'rgba(255, 159, 64, 0.2)'
                       ],
                       borderColor: [
                           'rgba(255, 99, 132, 1)',
                           'rgba(54, 162, 235, 1)',
                           'rgba(255, 206, 86, 1)',
                           'rgba(75, 192, 192, 1)',
                           'rgba(153, 102, 255, 1)',
                           'rgba(255, 159, 64, 1)'
                       ],
                       borderWidth: 1
                   }]
               },
               options: {
                   scales: {
                       yAxes: [{
                           ticks: {
                               beginAtZero: true
                           }
                       }]
                   }
               }
           });
       }
   </script>
@endsection
