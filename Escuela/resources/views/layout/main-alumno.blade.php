<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Course Project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield("titulo") - JS</title>
    <link rel="stylesheet" type="text/css" href="{{asset('styles/bootstrap4/bootstrap.min.css')}}">
    <link href="{{asset('plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('styles/main_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('styles/responsive.css')}}">
    @yield('css')
</head>
<body id="page-top" style="background-color: #E1F1FC">
<div class="super_container" >
    <header class="header d-flex flex-row">
        <div class="header_content d-flex flex-row align-items-center">
            <!-- Logo -->
            <div class="logo_container">
                <div class="logo">
                    <img src="{{asset('/images/logo.png')}}" alt="">
                    <span>Preparatoria Justo Sierra</span>
                </div>
            </div>
            <nav class="main_nav_container">
                <div class="main_nav">
                    <ul class="main_nav_list">
                        <li class="main_nav_item"><a href="{{route('informacionA')}}">Mi informacion</a></li>
                        <li class="main_nav_item"><a href="{{route('archivos')}}">Archivos</a></li>
                        <li class="main_nav_item"><a href="{{route('calificacionesA')}}">Calificaciones</a></li>
                        <li class="main_nav_item"><a href="{{route('cerrar.sesionA')}}">Salir</a></li>
                    </ul>
                </div>
            </nav>
            <div class="hamburger_container">
                <i class="fas fa-bars trans_200"></i>
            </div>
        </div>
    </header>
    <div class="menu_container menu_mm">
        <div class="logo_container" style="margin-top: 15%">
            <div class="logo">
                <img src="{{asset('mages/logo.png')}}i" alt="">
                <span>Preparatoria Justo Sierra</span>
            </div>
        </div>
        <div class="main_nav container">
            <ul class="col-lg-10" style="margin-top: 15%">
                <li class="main_nav_item"><a href="{{route('informacionA')}}">Mi informacion</a></li>
                <br>
                <li class="main_nav_item"><a href="{{route('archivos')}}">Subir archivos</a></li>
                <br>
                <li class="main_nav_item"><a href="{{route('calificacionesA')}}">Calificaciones</a></li>
                <br>
                <li class="main_nav_item"><a href="{{route('cerrar.sesionA')}}">Salir</a></li>
            </ul>
        </div>
        <div class="menu_close_container">
            <div class="menu_close"></div>
        </div>
    </div>
    <div class="popular page_section">
        <div class="container">
            <div class="row course_boxes">
                @yield('contenido')
            </div>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('styles/bootstrap4/popper.js')}}"></script>
<script src="{{asset('styles/bootstrap4/bootstrap.min.js')}}"></script>
<script src="{{asset('plugins/greensock/TweenMax.min.js')}}"></script>
<script src="{{asset('plugins/greensock/TimelineMax.min.js')}}"></script>
<script src="{{asset('plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
<script src="{{asset('plugins/greensock/animation.gsap.min.js')}}"></script>
<script src="{{asset('plugins/greensock/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{asset('plugins/scrollTo/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('plugins/easing/easing.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
@yield('js')
</body>
</html>

