<?php


namespace App\Http\Controllers;
use App\Models\materias;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class MateriasController
{
    public function materias (){
        $materias = materias::get();
        if($materias){
            echo json_encode(["estatus" => "success","tablero" => $materias]);
            return view("Alumno.calificaciones");
        }
        else{
            echo json_encode(["estatus" => "error"]);
            return view("Alumno.calificaciones");
        }
    }
}
