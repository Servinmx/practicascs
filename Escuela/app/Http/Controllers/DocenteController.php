<?php
namespace App\Http\Controllers;
use App\Models\Alumnos;
use App\Models\Docentes;
use App\Models\Materias;
use App\Models\Calificaciones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class DocenteController extends Docentes
{
    public function crear (Request $datos){
        if(!$datos -> nombre || !$datos -> apeidoP || !$datos -> apeidoM || !$datos -> cedula || !$datos -> correo || !$datos -> contrasenia1 || !$datos-> contrasenia2){
            return view("Docente.registro-docente", ["estatus"=> "error", "mensaje"=> "¡Falta información!"]);
        }

        $docente = Docentes::where('correo', $datos->correo)->first();
        if ($docente){
            return view("Docente.registro-docente",["estatus"=> "error", "mensaje"=> "¡El correo ya se encuentra registrado!"]);
        }
        $cedula = Docentes::where ('cedula', $datos -> cedula) -> first();
        if($cedula){
            return view("Docente.registro-docente",["estatus"=> "error", "mensaje"=> "¡La cedula ya se encuentra registrada!"]);
        }
        $correo = $datos -> correo;
        $contrasenia1 = $datos -> contrasenia1;
        $contrasenia2 = $datos -> contrasenia2;

        if($contrasenia1 != $contrasenia2){
            return view("Docente.registro-docente",["estatus" => "¡Las contraseñas son diferentes!"]);
        }
        else{
            $con = bcrypt($contrasenia1);
        }

        $info = new Docentes();
        $info -> nombre = $datos -> nombre;
        $info -> apeidoP = $datos -> apeidoP;
        $info -> apeidoM = $datos -> apeidoM;
        $info -> cedula = $datos -> cedula;
        $info -> correo = $correo;
        $info -> contrasenia = $con;
        $info -> save();
        return view("Docente.login-docente",["estatus"=> "success", "mensaje"=> "¡Cuenta Creada!"]);

    }

    public function credenciales (Request $datos){
        if (!$datos->correo || !$datos->contrasenia){
            return view("Docente.login-docente",["estatus"=> "error", "mensaje"=> "Campos incompletos"]);
        }
        $docente = Docentes::where("correo",$datos->correo)->first();
        if(!$docente){
            return view("Docente.login-docente",["estatus"=> "error", "mensaje"=> "¡El correo no esta registrado!"]);
        }
        if(!Hash::check($datos->contrasenia,$docente->contrasenia))
            return view("login",["estatus"=> "error", "mensaje"=> "¡Datos incorrectos!"]);

        Session::put('docente',$docente);
        if(!session('docente')){
            route('loginD');
        }

        if(isset($datos->url)){
            $url = decrypt($datos->url);
            return redirect($url);
        }else{
            return redirect()->route('informacionD');
        }
    }

    public function cerrarSesion(){
        if(Session::has('docente')) {
            Session::forget('docente');
        }
        return view('index');
    }

    public function editarDocente(Request $editar){
        $docente = Docentes::find($editar->id);
        if(!$editar -> nombre || !$editar -> apeidoP || !$editar -> apeidoM || !$editar -> cedula  || !$editar -> correo || !$editar -> contrasenia1 ){
            return view("Docente.informacion", ["estatus"=> "error", "mensaje"=> "¡Falta información!"]);
        }

        $con = bcrypt($editar->contrasenia1);

        $docente -> nombre = $editar->nombre;
        $docente -> apeidoP = $editar->apeidoP;
        $docente -> apeidoM = $editar->apeidoM;
        $docente -> cedula = $editar->cedula;
        $docente -> correo = $editar->correo;
        $docente -> contrasenia = $con;
        $docente -> save();
        return view("Docente.materias");
    }

    public function addMaterias(Request $materia){
        if(!$materia -> nombre || !$materia -> clave || !$materia -> horas){
            return view("Docente.materias", ["estatus"=> "error", "mensaje"=> "¡Falta información!"]);
        }
        $nombre = Materias::where ('nombre', $materia -> nombre) -> first();
        if($nombre){
            return view("Docente.materias",["estatus"=> "error", "mensaje"=> "¡La materia ya se encuentra registrada!"]);
        }
        else{
            $nombre = $materia -> nombre;
        }

        $clave = Materias::where ('clave', $materia -> clave) -> first();
        if($clave){
            return view("Docente.materias",["estatus"=> "error", "mensaje"=> "¡La cedula ya se encuentra registrada!"]);
        }
        else{
            $clave = $materia->clave;
        }
        $mat = new Materias();
        $mat -> nombre = $nombre;
        $mat -> clave = $clave;
        $mat -> horas = $materia -> horas;
        $mat -> save();
        return view("Docente.materias", ["estatus"=> "success", "mensaje"=> "¡Materia ".$nombre." registrada!"]);
    }

    public function graficas(){
        $materias = Materias::get();
        $calificaciones = Calificaciones::get();
        return view("Docente.materias-graficas",["materias" => $materias, "calificaciones"=>$calificaciones]);
    }

    public function generar (){
        $calificaciones = Calificaciones::select('calificacion')->get();
        $calificacionesID = Calificaciones::select('id')->get();
        //return response()->json($calificacionesID);
        $datos =[];
        $cont = 0;
        $p = Calificaciones::all();
        foreach ($p as $cal){
            $x =$cal->id_materia;
            $y =$cal->calificacion;

          $dato = $x.";".$y;
            array_push($datos,$dato);
        }
        return response()->json($datos);
    }

    public function alumnos(){
        $alumnos = Alumnos::get();
        return view("Docente.alumnos",["alumnos" => $alumnos]);
    }

    public function calificar(){
        $alumnos = Alumnos::get();
        $materias = Materias::get();
        return view("Docente.calificar",["alumnos" => $alumnos],["materias"=>$materias]);
    }

    public function calificaciones (Request $calificacion){
        $alumnos = Alumnos::get();
        $materias = Materias::get();
        $info1 = [];
        $info2 = [];
        $cont = 0;
        $contArreglo1 = 0;
        $contArreglo2 = 0;
            foreach ($calificacion->all() as $cal){
                if($cont != 0 && $cont != 2 && $cont != 1){
                   if( $cont %2 == 0){
                        $info1 [$contArreglo1] = $cal[0];
                       $contArreglo1 = $contArreglo1 +1;
                    }
                   else{
                       $info2 [$contArreglo2] = $cal[0];
                       $contArreglo2 = $contArreglo2 +1;
                   }
                }
                $cont = $cont +1;
            }
        for ($x=0; $x<count($info1); $x++){
            echo $info1[$x]. " ".$info2[$x];
            $calificaciones = new Calificaciones();
            $calificaciones -> id_alumno = $info2[$x];
            $calificaciones -> id_materia = $calificacion -> materia;
            $calificaciones -> calificacion = $info1[$x];
            $calificaciones -> save();
        }
        return view("Docente.calificar",["estatus"=> "success", "mensaje"=> "Materia calificada", "alumnos" => $alumnos, "materias"=>$materias]);

    }

    public function buscar(Request $alumno){
        $nombre = Alumnos::where ('nombre', $alumno -> nombre) -> first();
        if(!$nombre){
            return redirect()->route('alumnos');
        }
        $alumnos = Alumnos::get();
        $id = null;
        $nom = $alumno -> nombre;
        foreach ($alumnos as $a) {
            if($a -> nombre == $nom){
                $id = $a->id;
            }
        }
        $informacionA = Alumnos::find($id);
        print_r($id) ;
        return view("Docente.informacion-alumno",["informacionA" => $informacionA]);
    }

    public function loginD(){
        return view("Docente.login-docente");
    }
    public function registroD(){
        return view("Docente.registro-docente");
    }

    public function informacion(){
        return view("Docente.informacion");
    }
    public function materias(){
        return view("Docente.materias");
    }

}
