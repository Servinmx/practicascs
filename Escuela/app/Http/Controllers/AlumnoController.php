<?php
namespace App\Http\Controllers;
use App\Models\Calificaciones;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\Alumnos;
use App\Models\Materias;

class AlumnoController extends Alumnos
{
    public function crear (Request $datos){
        if(!$datos -> nombre || !$datos -> apeidoP || !$datos -> apeidoM || !$datos -> matricula  || !$datos -> grado || !$datos -> grupo || !$datos -> correo || !$datos -> contrasenia1 || !$datos-> contrasenia2){
            return view("Alumno.registro-alumno", ["estatus"=> "error", "mensaje"=> "¡Falta información!"]);
        }

        $alumno = Alumnos::where('correo', $datos->correo)->first();
        if ($alumno){
            return view("Alumno.registro-alumno",["estatus"=> "error", "mensaje"=> "¡El correo ya se encuentra registrado!"]);
        }
        $matricula = Alumnos::where ('matricula', $datos -> matricula) -> first();
        if($matricula){
            return view("Alumno.registro-alumno",["estatus"=> "error", "mensaje"=> "¡La matricula ya se encuentra registrada!"]);
        }
        $correo = $datos -> correo;
        $contrasenia1 = $datos -> contrasenia1;
        $contrasenia2 = $datos -> contrasenia2;

        if($contrasenia1 != $contrasenia2){
            return view("Alumno.registro-alumno",["estatus" => "¡Las contraseñas son diferentes!"]);
        }
        else{
            $con = bcrypt($contrasenia1);
        }

        $info = new Alumnos();
        $info -> nombre = $datos -> nombre;
        $info -> apeidoP = $datos -> apeidoP;
        $info -> apeidoM = $datos -> apeidoM;
        $info -> matricula = $datos -> matricula;
        $info -> grado = $datos -> grado;
        $info -> grupo = $datos -> grupo;
        $info -> correo = $correo;
        $info -> contrasenia = $con;
        $info -> save();
        return view("Alumno.login-alumno",["estatus"=> "success", "mensaje"=> "¡Cuenta Creada!"]);

    }

    public function credenciales (Request $datos){
        if (!$datos->correo || !$datos->contrasenia){
            return view("Alumno.login-alumno",["estatus"=> "error", "mensaje"=> "Campos incompletos"]);
        }
        $alumno = Alumnos::where("correo",$datos->correo)->first();
        if(!$alumno){
            return view("login",["estatus"=> "error", "mensaje"=> "¡El correo no esta registrado!"]);
        }
        if(!Hash::check($datos->contrasenia,$alumno->contrasenia))
            return view("login",["estatus"=> "error", "mensaje"=> "¡Datos incorrectos!"]);

        Session::put('alumno',$alumno);
        if(!session('alumo')){
            route('loginA');
        }
        session_start();
        $_SESSION ['alumno']=$alumno->id;

        if(isset($datos->url)){
            $url = decrypt($datos->url);
            return redirect($url);
        }else{
            return redirect()->route('informacionA');
        }

    }

    public function cerrarSesion(){
        if(Session::has('alumno'))
            Session::forget('alumno');

        return view('index');
    }

    public function editarAlumno(Request $editar)
    {
        $alumno = Alumnos::find($editar->id);
        if(!$editar -> nombre || !$editar -> apeidoP || !$editar -> apeidoM || !$editar -> matricula  || !$editar -> grado || !$editar -> grupo || !$editar -> correo || !$editar -> contrasenia1 ){
            return view("Alumno.informacion", ["estatus"=> "error", "mensaje"=> "¡Falta información!"]);
        }

        $con = bcrypt($editar->contrasenia1);
        $alumno -> nombre = $editar->nombre;
        $alumno -> apeidoP = $editar->apeidoP;
        $alumno -> apeidoM = $editar->apeidoM;
        $alumno -> matricula = $editar->matricula;
        $alumno -> grupo = $editar->grupo;
        $alumno -> grado = $editar->grado;
        $alumno -> promedio = $editar->promedio;
        $alumno -> correo = $editar->correo;
        $alumno -> contrasenia = $con;
        $alumno -> save();
        return view("Alumno.archivos");
    }

    public function materias (){
        $materias = Materias::get();
        $promedio = null;
        $suma = null;
        session_start();
        $cal = Calificaciones::where('id_alumno',$_SESSION ['alumno'])->get();
        $cont= Calificaciones::where('id_alumno',$_SESSION ['alumno'])->count();
        $id = null;
        foreach ($cal as $x){
            $suma = $suma + $x->calificacion;
            $id = $x->id_alumno;
        }
        //return $id;
        $promedio = $suma/$cont;
        $alumno = Alumnos::find($id);
        $alumno -> promedio = $promedio;
        $alumno -> save();
        return view("Alumno.calificaciones",["materias" => $materias, "calificaciones"=>$cal, "promedio"=>$promedio]);
    }
    public function loginA(){
        return view("Alumno.login-alumno");
    }

    public function registroA(){
        return view("Alumno.registro-alumno");
    }

    public function archivos (){
        return view("Alumno.archivos");
    }

    public function archivosA(Request $archivos){
        if(!$archivos->id || !$archivos->ine || !$archivos->curp || !$archivos->acta){
            return view("Alumno.archivos", ["estatus"=> "error", "mensaje"=> "¡Falta algun archivo!"]);
        }
        else{
            $arch = Alumnos::find($archivos->id);
            $arch -> ine = $archivos->file('ine')->store("public");
            $arch -> curp = $archivos->file('curp')->store("public");
            $arch -> acta = $archivos->file('acta')->store("public");
            $arch -> save();
            return view("Alumno.archivos", ["estatus"=> "success", "mensaje"=> "¡Archivos Guardados!"]);
        }
    }

    public function calificaciones(){
        return view("Alumno.calificaciones");
    }

    public function imprimir (){
        $materias = Materias::get();
        $promedio = null;
        $suma = null;
        session_start();
        $cal = Calificaciones::where('id_alumno',$_SESSION ['alumno'])->get();
        $cont= Calificaciones::where('id_alumno',$_SESSION ['alumno'])->count();
        foreach ($cal as $x){
            $suma = $suma + $x->calificacion;
        }
        //return $cal;
        $promedio = $suma/$cont;
        $pdf = \PDF::loadView('Alumno.PDF',["materias" => $materias, "calificaciones"=>$cal, "promedio"=>$promedio]);
        return $pdf->download('PDF.pdf');
    }
    public function informacion(){
        return view("Alumno.informacion");
    }
}
