<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\DocenteController;
use App\Http\Controllers\MateriasController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
//Libres
Route::get('/registroAlumno',[AlumnoController::class,'registroA'])->name('registroA');
Route::post('/registroAlumno',[AlumnoController::class,'crear'])->name('crearA');
Route::get('/loginAlumno',[AlumnoController::class,'loginA'])->name('loginA');
Route::post('/loginAlumno',[AlumnoController::class,'credenciales'])->name('credencialesA');
Route::get('/cerrarSesionA',[AlumnoController::class,'cerrarSesion'])->name('cerrar.sesionA');

Route::get('/loginDocente',[DocenteController::class,'loginD'])->name('loginD');
Route::post('/loginDocente',[DocenteController::class,'credenciales'])->name('credencialesD');
Route::post('/registroDocente',[DocenteController::class,'crear'])->name('crearD');
Route::get('/registroDocente',[DocenteController::class,'registroD'])->name('registroD');
Route::get('/cerrarSesionD',[DocenteController::class,'cerrarSesion'])->name('cerrar.sesionD');



Route::prefix('/alumno')->middleware("AlumnoVerificacion")->group(function (){
    Route::get('/archivos',[AlumnoController::class,'archivos'])->name('archivos');
    Route::get('/informacionA',[AlumnoController::class,'informacion'])->name('informacionA');
    Route::post('/editarAlumno',[AlumnoController::class,'editarAlumno'])->name('editarAlumno');
    Route::get('/calificacionesA',[AlumnoController::class,'materias'])->name('calificacionesA');
    Route::get('/archivos',[AlumnoController::class,'archivos'])->name('archivos');
    Route::post('/archivosA',[AlumnoController::class,'archivosA'])->name('archivosA');
    Route::get('/imprimir',[AlumnoController::class,'imprimir'])->name('pdf');


});

Route::prefix('/docente')->middleware("DocenteVerificacion")->group(function () {
    Route::get('/alumnos', [DocenteController::class, 'alumnos'])->name('alumnos');
    Route::get('/calificar', [DocenteController::class, 'calificar'])->name('calificar');
    Route::post('/calificaciones', [DocenteController::class, 'calificaciones'])->name('calificaciones');
    Route::get('/informacionD', [DocenteController::class, 'informacion'])->name('informacionD');
    Route::post('/editarDocente', [DocenteController::class, 'editarDocente'])->name('editarDocente');
    Route::get('/materias', [DocenteController::class, 'materias'])->name('materias');
    Route::post('/addMateria', [DocenteController::class, 'addMaterias'])->name('addMateria');
    Route::get('/graficas', [DocenteController::class, 'graficas'])->name('graficas');
    Route::get('/generar', [DocenteController::class, 'generar'])->name('generar');
    Route::post('/buscarA', [DocenteController::class, 'buscar'])->name('buscarA');
});















