<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('index');
});

Route::get('/home',[UsuarioController::class,'index'])->name('index');
Route::get('/prueba/{numero}', [AdminController::class, 'prueba'])->name('prueba');

//Login y Registro
Route::get('/login',[UsuarioController::class,'login'])->name('login');
Route::post('/login',[UsuarioController::class,'iniciarSecion'])->name('validar.usuario');
Route::get('/registro',[UsuarioController::class,'registro'])->name('registrar');
Route::post('/registro',[UsuarioController::class,'registroValidar'])->name('registrar.usuario');
Route::get('/cerrarSesion',[UsuarioController::class,'cerrarSesion'])->name('cerrar.sesion');

Route::prefix('/usuario')->middleware("UsuarioIniciado")->group(function () {
    Route::get('/inicio',[UsuarioController::class,'inicio'])->name('usuario.inicio');
    Route::get('/productoFut/{id}/',[UsuarioController::class,'verProductoFutbol'])->name('ver.producto.futbol');
    Route::get('/productoCor/{id}/',[UsuarioController::class,'verProductoCorrer'])->name('ver.producto.correr');
    Route::get('/productoFit/{id}/',[UsuarioController::class,'verProductoFitness'])->name('ver.producto.fitness');
    Route::get('/futbol',[UsuarioController::class,'futbol'])->name('categoria.futbol');
    Route::get('/correr',[UsuarioController::class,'correr'])->name('categoria.correr');
    Route::get('/fitness',[UsuarioController::class,'fitness'])->name('categoria.fitness');
    Route::get('/carrito', [UsuarioController::class, 'verCarrito'])->name('ver.mi.carrito');
    Route::get('/carritoV', [UsuarioController::class, 'carritoVer'])->name('ver.el.carrito');
    Route::post('/carrito',[UsuarioController::class,'agregarCarrito'])->name('agregar.carrito');
    Route::get('/comprarC',[UsuarioController::class,'compraCarrito'])->name('comprar.carrito');
    Route::get('/comprarCar',[UsuarioController::class,'carritoComprado'])->name('carrito.comprado');
    Route::post('/comprar',[UsuarioController::class,'compraProducto'])->name('comprar.producto');
    Route::get('/comprar',[UsuarioController::class,'productoComprado'])->name('producto.comprado');
    Route::get('/compras',[UsuarioController::class,'compras'])->name('compras.hechas');
    Route::get('/comprasV', [UsuarioController::class, 'comprasVer'])->name('ver.las.compras');
});

Route::get('/admin',[AdminController::class,'loginadmin'])->name('login.admin');
Route::post('/loginadmin',[AdminController::class,'revisarAdmin'])->name('revisar.admin');
Route::get('/registroE', [AdminController::class, 'registrarAdmin'])->name('registrar.admin');
Route::post('registroE', [AdminController::class, 'registroAdmin'])->name('registro.admin');
Route::get('/cerrarSesionE',[AdminController::class,'cerrarSesionAdmin'])->name('cerrar.sesion.admin');

Route::prefix('/administrador')->middleware("AdminIniciado")->group(function () {
    Route::get('/inicio',[AdminController::class,'admin'])->name('administrador.inicio');
    Route::get('/agregar',[AdminController::class,'agregar'])->name('administrador.agregar');
    Route::post('/agregar',[AdminController::class,'agregarProducto'])->name('registrar.producto');
    Route::get('/futbol', [AdminController::class,'futbol'])->name('ver.futbol');
    Route::get('/correr', [AdminController::class,'correr'])->name('ver.correr');
    Route::get('/fitness', [AdminController::class,'fitness'])->name('ver.fitness');
    Route::get('/pedidos',[AdminController::class,'pedidos'])->name('ver.pedidos.tienda');
    Route::get('/pedidosV',[AdminController::class,'verPedidos'])->name('ver.los.pedidos');
});

