@extends('layout.main')

@section('titulo')
    <title>Pedidos | Administrador</title>
@endsection
@section('sesionUs')
    Cerrar Sesion Admin
@endsection
<!--Cantidad de cosas guardas en el carrito-->
@section('carrito')
    <div class="qty">0</div>
@endsection
<!--Cantidad de "Pedidos"-->
@section('compras')
    <div class="qty">0</div>
@endsection
<!-- Cantidad de cosas en el carrito "vistaL -->
@section('cantidadCarrito')
    <small>0 Cosa(s) en su Carrito</small>
@endsection
@section('admin')
    <li><a href="{{route('administrador.agregar')}}">AGREGAR PRODUCTO</a></li>
    <li><a href="{{route('ver.pedidos.tienda')}}">PEDIDOS</a></li>
@endsection
@section('contenido')
    <div class="col-md-12 row titulo">
        <h1>Pedidos de Toda la Tienda</h1>
    </div>
    <div class="col-md-12 totalCarrito">
        <p>Lista de todos los pedidos realizados para la tienda.</p>
        <br>
    </div>
    <div class="container">
        <br><br>
        <div class="row">
            <!--Tabla de Agenda de Lunes -->
            <table class="table table-striped" style="width:100%" id="tableCarrito">
                <thead>
                <tr>
                    <th>Usuario</th>
                    <th>Producto</th>
                    <th>Precio</th>
                    <th>Categoria</th>
                    <th>Cantidad</th>
                    <th>Total</th>
                    <th>Referencia</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
@section('js')
    <!-- Ajax --> <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
    <!-- Jquery --> <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <!-- DataTables --> <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#tableCarrito').DataTable({
                "processing": true,
                "ajax": "{{ route('ver.los.pedidos')}}",
                "columns": [
                    {data: 'usuario'},
                    {data: 'producto'},
                    {data: 'precio'},
                    {data: 'categorias'},
                    {data: 'cantidad'},
                    {data: 'total'},
                    {data: 'referencia'}
                ]
            });

        });
    </script>
@endsection
