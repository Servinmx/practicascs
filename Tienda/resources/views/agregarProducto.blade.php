<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Registro</title>
    <!-- CSS -->
    <!-- Bootstrap --> <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- Ajax --> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />

    <link rel="stylesheet" href="{{ asset('css/registro.css')}}">
</head>
<body>
<div class="container">
    <div class="row text-center login-page">
        <div class="col-md-12 login-form">
            <form action="{{route('registrar.producto')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row present">
                    <div class="col-md-12">
                        <p class="login-form-font-header"><span>Sport City</span><p>
                        <p class="login-form-font-header2">Agregar un Nuevo Producto </p>
                    <label class="text-danger">
                        @if(isset($estatus))
                            @if($estatus != "foto1" && $estatus != "foto2" && $estatus != "foto3" && $estatus != "foto4"
                                && $estatus != "foto5")
                                @if($estatus != "aprobado")
                                    <label class="text-danger">{{$mensaje}}</label>
                                @else
                                    <label class="text-success">{{$mensaje}}</label>
                                @endif

                            @endif
                        @endif
                    </label>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2 row">
                            <div class="col-md-4">
                                <label for="nombre" class="form-label">Nombre:<span>*</span></label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" maxlength="30">
                            </div>
                            <div class="col-md-4">
                                <label for="precio" class="form-label">Precio:<span>*</span></label>
                                <input type="text" class="form-control" id="precio" name="precio" placeholder="Precio" maxlength="30">
                            </div>
                            <div class="col-md-4">
                                <label for="precioE" class="form-label">Descuento:</label>
                                <select class="form-control" name="precioE" id="precioE">
                                    <option value="0">Sin Descuento</option>
                                    <option value="5">5%</option>
                                    <option value="10">10%</option>
                                    <option value="15">15%</option>
                                    <option value="20">20%</option>
                                    <option value="25">25%</option>
                                    <option value="30">30%</option>
                                    <option value="35">35%</option>
                                    <option value="40">40%</option>
                                    <option value="45">45%</option>
                                    <option value="50">50%</option>
                                    <option value="60">60%</option>
                                    <option value="70">70%</option>
                                    <option value="80">80%</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2 row">
                            <div class="col-md-12">
                                <label for="descripcion" class="form-label">Descripcion:<span>*</span></label>
                                <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Describe el Producto" maxlength="100">
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2 row">
                            <div class="col-md-6">
                                <label for="categoria" class="form-label">Categoria:<span>*</span></label>
                                <select class="form-control" name="categoria" id="categoria">
                                    <option value="futbol">Fut bol</option>
                                    <option value="correr">Correr</option>
                                    <option value="fitness">Fitness</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="color" class="form-label">Color:<span>*</span></label>
                                <input type="text" class="form-control" id="color" name="color" placeholder="Color del Producto" maxlength="20">
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2 row">
                            <div class="col-md-12">
                                <label for="detalles" class="form-label">Detalles:<span>*</span></label>
                                <input type="text" class="form-control" id="detalles" name="detalles" placeholder="Detalles del Producto" maxlength="100">
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2 row">
                            <div class="col-md-6 row">
                                <label for="foto1" class="form-label">Agregue la Imagen 1 </label>
                                <label class="text-danger">
                                    @if(isset($mensaje))
                                        @if($estatus == "foto1")
                                            <label class="text-danger">{{$mensaje}}</label>
                                        @endif
                                    @endif
                                </label>
                                <input class="form-control" type="file" id="foto1" name="foto1">
                            </div>
                            <div class="col-md-6 row">
                                <label for="foto2" class="form-label">Agregue la Imagen 2 </label>
                                <label class="text-danger">
                                    @if(isset($mensaje))
                                        @if($estatus == "foto2")
                                            <label class="text-danger">{{$mensaje}}</label>
                                        @endif
                                    @endif
                                </label>
                                <input class="form-control foto" type="file" id="foto2" name="foto2">
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2 row">
                            <div class="col-md-6 row">
                                <label for="foto3" class="form-label">Agregue la Imagen 3 </label>
                                <label class="text-danger">
                                    @if(isset($mensaje))
                                        @if($estatus == "foto3")
                                            <label class="text-danger">{{$mensaje}}</label>
                                        @endif
                                    @endif
                                </label>
                                <input class="form-control" type="file" id="foto3" name="foto3">
                            </div>
                            <div class="col-md-6 row">
                                <label for="foto4" class="form-label">Agregue la Imagen 4 </label>
                                <label class="text-danger">
                                    @if(isset($mensaje))
                                        @if($estatus == "foto4")
                                            <label class="text-danger">{{$mensaje}}</label>
                                        @endif
                                    @endif
                                </label>
                                <input class="form-control foto" type="file" id="foto4" name="foto4">
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2 row">
                            <div class="col-md-6 offset-md-3 row">
                                <label for="foto5" class="form-label">Agregue la Imagen 5 </label>
                                <label class="text-danger">
                                    @if(isset($mensaje))
                                        @if($estatus == "foto5")
                                            <label class="text-danger">{{$mensaje}}</label>
                                        @endif
                                    @endif
                                </label>
                                <input class="form-control" type="file" id="foto5" name="foto5">
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2 row">
                            <div class="col-md-6 offset-md-3 row" id="enviar">
                                <input type="submit" class="btn btn-primary btn-user btn-block" value="Agregar Producto">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="text-center" id="login">
                <a class="small" href="{{route('administrador.inicio')}}">Regresar al Inicio</a>
            </div>
            <br><br>
        </div>
    </div>
</div>

<!-- JS -->
<!-- Jquery --> <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
<!-- Ajax --> <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
</body>
</html>
