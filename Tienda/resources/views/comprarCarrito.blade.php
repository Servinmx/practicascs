@extends('layout.comprar')

@section('compras')
    @foreach($cosasCarrito as $carritos)
    <div class="order-col">
        <div>{{$carritos->cantidad}}x {{$carritos->producto}}</div>
        <div>${{$carritos->total}}.00</div>
    </div>
    @endforeach
@endsection
@section('totalcompra')
    @if(isset($totalCarrito))
    <div><strong class="order-total">${{$totalCarrito}}.00</strong></div>
    @endif
@endsection
@section('botonComprar')
    <a href="{{route('carrito.comprado')}}" class="primary-btn order-submit">Comprar</a>
@endsection
