@extends('layout.productos')

@section('css')

@endsection
<!--Titulo de Pagina-->
@section('titulo')
    <title>Fut Bol | Sport City</title>
@endsection
<!--Cerrar secion de "Usuario"-->
@section('sesionUs')
    @if(isset($usuario))
        Cerrar Sesion Admin
    @else
        Cerrar Sesion
    @endif
@endsection
@section('admin')
    <li><a href="{{route('administrador.agregar')}}">AGREGAR PRODUCTO</a></li>
    <li><a href="{{route('ver.pedidos.tienda')}}">PEDIDOS</a></li>
@endsection
@section('migaja')
    <!-- BREADCRUMB -->
    <div id="breadcrumb" class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb-tree">
                        @if(isset($usuario))
                            <li><a href="{{route('administrador.inicio')}}">Inicio</a></li>
                        @else
                            <li><a href="{{route('usuario.inicio')}}">Inicio</a></li>
                        @endif
                        @if(isset($categoria))
                            <li class="active">{{$categoria->categorias}}</li>
                        @endif
                    </ul>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /BREADCRUMB -->
@endsection
@section('filtrarPor')
    <div class="input-checkbox">
        <input type="checkbox" id="category-1">
        <label for="category-1">
            <span></span>
            Calzado
        </label>
    </div>

    <div class="input-checkbox">
        <input type="checkbox" id="category-2">
        <label for="category-2">
            <span></span>
            Ropa
        </label>
    </div>

    <div class="input-checkbox">
        <input type="checkbox" id="category-3">
        <label for="category-3">
            <span></span>
            Accesorios
        </label>
    </div>

    <div class="input-checkbox">
        <input type="checkbox" id="category-4">
        <label for="category-4">
            <span></span>
            Equipamiento
        </label>
    </div>
@endsection

@section('marca')
    <div class="input-checkbox">
        <input type="checkbox" id="brand-1">
        <label for="brand-1">
            <span></span>
            Nike
        </label>
    </div>
    <div class="input-checkbox">
        <input type="checkbox" id="brand-2">
        <label for="brand-2">
            <span></span>
            Adidas
        </label>
    </div>
    <div class="input-checkbox">
        <input type="checkbox" id="brand-3">
        <label for="brand-3">
            <span></span>
            Puma
        </label>
    </div>
    <div class="input-checkbox">
        <input type="checkbox" id="brand-3">
        <label for="brand-3">
            <span></span>
            Puma
        </label>
    </div>
    <div class="input-checkbox">
        <input type="checkbox" id="brand-3">
        <label for="brand-3">
            <span></span>
            Reebok
        </label>
    </div>
@endsection
@section('producto')
    @foreach($productos as $producto)
    <!-- product -->
    <div class="col-md-4 col-xs-6">
        <div class="product">
            <div class="product-img">
                <img src="{{ asset('./img/futbol/'.$producto->foto1)}}" alt="">
                <div class="product-label">
                    @if($producto->descuento == "0")
                        <span class="new">Nuevo</span>
                    @else
                        <span class="sale">-{{$producto->descuento}}%</span>
                        <span class="new">Nuevo</span>
                    @endif
                </div>
            </div>
            <div class="product-body">
                <p class="product-category">Fut Bol</p>
                <h3 class="product-name"><a href="#">{{$producto->nombre}}</a></h3>
                @if($producto->precioEs == "0")
                    <h4 class="product-price">${{$producto->precio}}.00</h4>
                @else
                    <h4 class="product-price">${{$producto->precioEs}}.00 <del class="product-old-price">${{$producto->precio}}.00</del></h4>
                @endif
                <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
                <div class="product-btns">
                    <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">Agregar a Favoritos</span></button>
                    <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">Comparar</span></button>
                    <button class="quick-view"><i class="fa fa-share-alt"></i><span class="tooltipp">Compatir</span></button>
                </div>
            </div>
            <div class="add-to-cart">
                @if(isset($usuario))
                    <a href="#">
                        <button class="add-to-cart-btn" data-bs-toggle="modal" data-bs-target="#agregarCarrito">
                            Ver Detalles</button></a>
                @else
                <a href="{{route('ver.producto.futbol',$producto->id)}}">
                <button class="add-to-cart-btn" data-bs-toggle="modal" data-bs-target="#agregarCarrito">
                    Ver Detalles</button></a>
                @endif
            </div>
        </div>
    </div>
    <!-- /product -->
    @endforeach
@endsection
@section('js')
@endsection
