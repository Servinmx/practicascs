@extends('layout.main')
<!--Titulo de Pagina-->
@section('titulo')
    <title>Comprar | Sport City</title>
@endsection
<!--Cerrar secion de "Usuario"-->
@section('sesionUs')
    @if(isset($usuario))
        Cerrar Sesion Admin
    @else
        Cerrar Sesion
    @endif
@endsection
@section('contenido')
    <!-- BREADCRUMB -->
    <div id="breadcrumb" class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="breadcrumb-header">Comprar</h3>
                    <ul class="breadcrumb-tree">
                        <li><a href="{{route('usuario.inicio')}}">Inicio</a></li>
                        <li class="active">Comprar</li>
                    </ul>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /BREADCRUMB -->

    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <div class="col-md-7">
                    <!-- Billing Details -->
                    <div class="billing-details">
                        <div class="section-title">
                            <h3 class="title">Direccion de Envio</h3>
                        </div>
                        <div class="form-group">
                            <input class="input" type="text" name="first-name" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input class="input" type="text" name="last-name" placeholder="Apellidos">
                        </div>
                        <div class="form-group">
                            <input class="input" type="email" name="email" placeholder="Correo">
                        </div>
                        <div class="form-group">
                            <input class="input" type="text" name="address" placeholder="Direccion">
                        </div>
                        <div class="form-group">
                            <input class="input" type="text" name="city" placeholder="Ciudad">
                        </div>
                        <div class="form-group">
                            <input class="input" type="text" name="country" placeholder="Estado">
                        </div>
                        <div class="form-group">
                            <input class="input" type="text" name="zip-code" placeholder="Codigo Postal">
                        </div>
                        <div class="form-group">
                            <input class="input" type="tel" name="tel" placeholder="Telefono">
                        </div>
                        <div class="form-group">
                            <div class="input-checkbox">
                                <input type="checkbox" id="create-account">
                                <label for="create-account">
                                    <span></span>
                                    Agregar Especificaciones
                                </label>
                                <div class="caption">
                                    <p>Escriba espeficiaciones extras, o referencias para la direccion de envio</p>
                                    <div class="order-notes">
                                        <textarea class="input" placeholder="Especificaciones"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Billing Details -->
                </div>

                <!-- Order Details -->
                <div class="col-md-5 order-details">
                    <div class="section-title text-center">
                        <h3 class="title">Tu Orden de Compra</h3>
                    </div>
                    <div class="order-summary">
                        <div class="order-col">
                            <div><strong>Productos</strong></div>
                            <div><strong>Total</strong></div>
                        </div>
                        <div class="order-products">
                            @yield('compras')
                        </div>
                        <div class="order-col">
                            <div>Envio</div>
                            <div><strong>GRATIS</strong></div>
                        </div>
                        <div class="order-col">
                            <div><strong>TOTAL</strong></div>
                            @yield('totalcompra')
                        </div>
                    </div>
                    <div class="payment-method">
                        <div class="input-radio">
                            <input type="radio" name="payment" id="payment-1">
                            <label for="payment-1">
                                <span></span>
                                Transferencia Bancaria
                            </label>
                            <div class="caption">
                                <p>Le llegara una peticion de compra a la aplicacion de su Banco.</p>
                            </div>
                        </div>
                        <div class="input-radio">
                            <input type="radio" name="payment" id="payment-2">
                            <label for="payment-2">
                                <span></span>
                                Efectivo
                            </label>
                            <div class="caption">
                                <p>Le llegara un correo con un codigo para poder pagar en el lugar de su
                                    preferencia.</p>
                            </div>
                        </div>
                    </div>
                    <div class="input-checkbox">
                        <input type="checkbox" id="terms">
                        <label for="terms">
                            <span></span>
                            He leído y acepto los <a href="#">términos y condiciones.</a>
                        </label>
                    </div>
                    @yield('botonComprar')
                </div>
                <!-- /Order Details -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->
@endsection
