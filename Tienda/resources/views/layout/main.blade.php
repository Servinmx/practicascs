<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    @yield('titulo')

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}"/>

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/slick.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/slick-theme.css')}}"/>

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/nouislider.min.css')}}"/>

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>
    @yield('css')
    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/style.css')}}"/>
    <!-- Ajax --> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- HEADER -->
<header>
    <!-- TOP HEADER -->
    <div id="top-header">
        <div class="container">
            <ul class="header-links pull-left">
                <li><a href="#"><i class="fa fa-phone"></i> +52 55-95-51-05-84</a></li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> contacto@sportcity.com</a></li>
                <li><a href="https://goo.gl/maps/iWfpDZX4iv6khji2A"><i class="fa fa-map-marker"></i> 55740 Power Center</a></li>
            </ul>
            <ul class="header-links pull-right">
                @if(isset($usuario) || $admin == "Admin")
                    <li><a href="{{route('cerrar.sesion.admin')}}"><i class="fa fa-user-o"></i> @yield('sesionUs')</a></li>
                @else
                    <li><a href="{{route('cerrar.sesion')}}"><i class="fa fa-user-o"></i> @yield('sesionUs')</a></li>
                @endif
            </ul>
        </div>
    </div>
    <!-- /TOP HEADER -->

    <!-- MAIN HEADER -->
    <div id="header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- LOGO -->
                <div class="col-md-3">
                    <div class="header-logo">
                        @if(isset($usuario) || $admin == "Admin")
                            <a href="{{route('administrador.inicio')}}" class="logo">
                                <img src="{{ asset('./img/logo.png')}}" alt="Logo de Sport City" width="280px" height="80px">
                            </a>
                        @else
                            <a href="{{route('usuario.inicio')}}" class="logo">
                                <img src="{{ asset('./img/logo.png')}}" alt="Logo de Sport City" width="280px" height="80px">
                            </a>
                        @endif
                    </div>
                </div>
                <!-- /LOGO -->

                <!-- SEARCH BAR -->
                <div class="col-md-6">
                    <div class="header-search">
                        <form>
                            <select class="input-select">
                                <option value="0">Toda Categoria</option>
                                <option value="1">Damas</option>
                                <option value="1">Caballeros</option>
                            </select>
                            <input class="input" placeholder="Articulo Deportivo">
                            <button class="search-btn">Buscar</button>
                        </form>
                    </div>
                </div>
                <!-- /SEARCH BAR -->

                <!-- ACCOUNT -->
                <div class="col-md-3 clearfix">
                    <div class="header-ctn">
                        <!-- Cart -->
                        <div class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Carrito</span>
                                @if(isset($usuario) || $admin == "Admin")
                                    <div class="qty">0</div>
                                @else
                                    <!--Cantidad de cosas guardas en el carrito-->
                                    @if(isset($carrito))
                                        <div class="qty">{{$carrito}}</div>
                                    @else
                                        <div class="qty">0</div>
                                    @endif
                                @endif
                            </a>
                            <!-- DESCRIPCION DEL CARRITO -->
                            <div class="cart-dropdown">
                                @if(isset($usuario) || $admin == "Admin")
                                    <div class="cart-summary">
                                        <small>0 Cosas en su Carrito</small>
                                    </div>
                                    <div class="cart-btns">
                                        <a href="#">Ver Carrito</a>
                                        <a href="#">Comprar <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                @else
                                    @if($cosasCarrito != "0")
                                    <div class="cart-list">
                                        @foreach($cosasCarrito as $carro)
                                            <div class="product-widget">
                                            <div class="product-img">
                                                <img src="{{asset('./img/'.$carro->categorias.'/'.$carro->foto)}}"
                                                     alt="" width="50px">
                                            </div>
                                            <div class="product-body">
                                                <h3 class="product-name"><a href="#">{{$carro->producto}}</a></h3>
                                                <h4 class="product-price">Cantidad: <span class="qty">x{{$carro->cantidad}}</span></h4>
                                                <h4 class="product-price">Precio: <span class="qty">${{$carro->precio}}.00</span></h4>
                                                <h4 class="product-price">Total: <span class="qty">${{$carro->total}}.00</span></h4>
                                            </div>
                                            <button class="delete"><i class="fa fa-close"></i></button>
                                            </div>
                                        @endforeach
                                    </div>
                                    @endif
                                    <div class="cart-summary">
                                        @if($carrito == 1)
                                            <small>{{$carrito}} Cosa en su Carrito</small>
                                        @else
                                            <small>{{$carrito}} Cosas en su Carrito</small>
                                        @endif
                                    </div>
                                    <div class="cart-btns">
                                        @if($carrito == 0)
                                            <a href="#">Ver Carrito</a>
                                            <a href="#">Comprar <i class="fa fa-arrow-circle-right"></i></a>
                                        @else
                                        <a href="{{route('ver.mi.carrito')}}">Ver Carrito</a>
                                        <a href="{{route('comprar.carrito')}}">Comprar <i class="fa fa-arrow-circle-right"></i></a>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!-- /Cart -->
                        <!-- Bag -->
                        <div>
                            @if(isset($usuario))
                                <a href="#">
                                    <i class="fa fa-shopping-bag"></i>
                                    <span>Compras</span>
                                    <!--Cantidad de "Pedidos"-->
                                    <div class="qty">0</div>
                                </a>
                            @else
                            <a href="{{route('compras.hechas')}}">
                                <i class="fa fa-shopping-bag"></i>
                                <span>Compras</span>
                                <!--Cantidad de "Pedidos"-->
                                @if(isset($carrito))
                                    <div class="qty">{{$compras}}</div>
                                @else
                                    <div class="qty">0</div>
                                @endif
                            </a>
                            @endif
                        </div>
                        <!-- /Bag -->

                        <!-- Menu Toogle -->
                        <div class="menu-toggle">
                            <a href="#">
                                <i class="fa fa-bars"></i>
                                <span>Menu</span>
                            </a>
                        </div>
                        <!-- /Menu Toogle -->
                    </div>
                </div>
                <!-- /ACCOUNT -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </div>
    <!-- /MAIN HEADER -->
</header>
<!-- /HEADER -->

<!-- NAVIGATION -->
<nav id="navigation">
    <!-- container -->
    <div class="container">
        <!-- responsive-nav -->
        <div id="responsive-nav">
            <!-- NAV -->
            <ul class="main-nav nav navbar-nav">
                @if(isset($usuario) || $admin == "Admin")
                    <li class="active"><a href="{{route('administrador.inicio')}}">Inicio</a></li>
                @else
                    <li class="active"><a href="{{route('usuario.inicio')}}">Inicio</a></li>
                @endif
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        Deportes
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @if(isset($usuario) || $admin == "Admin")
                            <li><a href="{{route('ver.futbol')}}">Fut Bol</a></li>
                            <li><a href="{{route('ver.correr')}}">Correr</a></li>
                            <li><a href="{{route('ver.fitness')}}">Fitness</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Otros Deportes</a></li>
                        @else
                            <li><a href="{{route('categoria.futbol')}}">Fut Bol</a></li>
                            <li><a href="{{route('categoria.correr')}}">Correr</a></li>
                            <li><a href="{{route('categoria.fitness')}}">Fitness</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Otros Deportes</a></li>
                        @endif
                    </ul>
                </li>
                <li><a href="#">Damas</a></li>
                <li><a href="#">Caballeros</a></li>
                <li><a href="#">Ropa</a></li>
                <li><a href="#">Calzado</a></li>
                <li><a href="#">Accesorios</a></li>
                    @if(isset($usuario) || $admin == "Admin")
                        @yield('admin')
                    @endif
            </ul>
            <!-- /NAV -->
        </div>
        <!-- /responsive-nav -->
    </div>
    <!-- /container -->
</nav>
<!-- /NAVIGATION -->
@yield('contenido')
<!-- NEWSLETTER -->
<div id="newsletter" class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <div class="newsletter">
                    <p>Regístrese para obtener <strong>Las Mejores Ofertas</strong></p>
                    <form>
                        <input class="input" type="email" placeholder="Ingrese su Correo Electronico">
                        <button class="newsletter-btn"><i class="fa fa-envelope"></i> Suscribirse</button>
                    </form>
                    <ul class="newsletter-follow">
                        <li>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /NEWSLETTER -->

<!-- FOOTER -->
<footer id="footer">
    <!-- top footer -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Sport City</h3>
                        <p>Sport City, una de las tiendas de Deporte mas popular del momento</p>
                        <ul class="footer-links">
                            <li><a href="#"><i class="fa fa-map-marker"></i>55749 Power Center</a></li>
                            <li><a href="#"><i class="fa fa-phone"></i>+52 55-95-51-05-84</a></li>
                            <li><a href="#"><i class="fa fa-envelope-o"></i>contacto@sportcity.com</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Categorias</h3>
                        <ul class="footer-links">
                            <li><a href="#">Dama</a></li>
                            <li><a href="#">Caballero</a></li>
                            <li><a href="#">Ropa</a></li>
                            <li><a href="#">Calzado</a></li>
                            <li><a href="#">Accesorios</a></li>
                        </ul>
                    </div>
                </div>

                <div class="clearfix visible-xs"></div>

                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Informacion</h3>
                        <ul class="footer-links">
                            <li><a href="#">Sobre Nosotros</a></li>
                            <li><a href="#">Ponerse en Contacto</a></li>
                            <li><a href="#">Politicas de Privacidad</a></li>
                            <li><a href="#">Terminos y Condiciones</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Servicios</h3>
                        <ul class="footer-links">
                            <li><a href="#">Mi cuenta</a></li>
                            <li><a href="#">Ver Carrito</a></li>
                            <li><a href="#">Ver Compras</a></li>
                            <li><a href="#">Ayuda</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /top footer -->

    <!-- bottom footer -->
    <div id="bottom-footer" class="section">
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="footer-payments">
                        <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                        <li><a href="#"><i class="fa fa-credit-card"></i></a></li>
                        <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
                        <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                        <li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
                        <li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
                    </ul>
                    <span class="copyright">
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
								Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                        Todos los derechos reservados | Equipo Dinamita UPT
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							</span>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /bottom footer -->
</footer>
<!-- /FOOTER -->

<!-- jQuery Plugins -->
<script src="{{ asset('js/jquery.min.js')}}"></script>
<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/slick.min.js')}}"></script>
<script src="{{ asset('js/nouislider.min.js')}}"></script>
<script src="{{ asset('js/jquery.zoom.min.js')}}"></script>
<script src="{{ asset('js/main.js')}}"></script>
@yield('js')
</body>
</html>
