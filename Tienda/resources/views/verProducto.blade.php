@extends('layout.producto')

@section('titulo')
    <title>{{$producto->nombre}} | Sport City</title>
@endsection
@section('migaja')
    <!-- BREADCRUMB -->
    <div id="breadcrumb" class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb-tree">
                        <li><a href="{{route('usuario.inicio')}}">Inicio</a></li>
                        <li><a href="{{route('categoria.'.$producto->categorias)}}">{{$producto->categorias}}</a></li>
                        <li class="active">{{$producto->nombre}}</li>
                    </ul>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /BREADCRUMB -->
@endsection
