<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Carrito;
use App\Models\Compras;
use App\Models\Productos;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    public function loginadmin(){
        return view('loginAdmin');
    }
    public function revisarAdmin(Request $informacion){
        //Valida que se escriba el correo y la contraseña
        if(!$informacion->correo || !$informacion->password){
            return view("loginAdmin",["estatus"=> "error", "mensaje"=> "¡Ingresa tu correo y contraseña!"]);
        }
        //Registrar desde 0
        if(($informacion->correo == "registrar@admin.com") && ($informacion->password == "ADMIN123")){
            return redirect()->route('registrar.admin');
        }
        //Busca que el Correo este registrado
        $admin = Admin::where("correo",$informacion->correo)->first();
        if(!$admin){
            return view("loginAdmin",["estatus"=> "error", "mensaje"=> "¡No se encontro su Correo, Contacte a su Jefe!"]);
        }
        //Verifica la Contraseña del Usuario
        if(!Hash::check($informacion->password,$admin->password)){
            return view("loginAdmin",["estatus"=> "error", "mensaje"=> "¡Contraseña Incorrecta!"]);
        }
        //Valida que se inicioe Secion
        Session::put('admin',$admin);
        //Creamos la Variable de Secion dentro para usar los datos
        session_start();
        $_SESSION['admin'] = $informacion->correo;
        $_SESSION['cargo'] = $admin->cargo;
        //Redireccionamos a la Pagina Principal
        return redirect()->route('administrador.inicio');
    }
    public function cerrarSesionAdmin(){
        if(Session::has('admin'))
            Session::forget('admin');
        return redirect()->route('index');
    }
    public function registrarAdmin(){
        return view('registroAdmin');
    }
    public function admin(){
        return view('admin', ["usuario"=> "Admin"]);
    }
    public function registroAdmin(Request $informacion){
        //Valida que se hayan llenado todos los datos
        if(!$informacion->nombre || !$informacion->aPaterno || !$informacion->aMaterno || !$informacion->cargo
            || !$informacion->correo || !$informacion->confcorreo || !$informacion->password || !$informacion->confpassword){
            return view("registroAdmin",["estatus"=> "error", "mensaje"=> "¡Complete todos los campos Obligatorios!"]);
        }
        //Valida que el correo no exista en la base de datos
        $usuario = Admin::where('correo',$informacion->email)->first();
        if($usuario == true){
            return view("registroAdmin",["estatus"=> "error", "mensaje"=> "Ya existe una cuenta con el Correo Ingresado"]);
        }
        //Se extraen todas las variables del formulario
        $nombre = $informacion->nombre;
        $aPaterno = $informacion->aPaterno;
        $aMaterno = $informacion->aMaterno;
        $cargo = $informacion->cargo;
        $correo = $informacion->correo;
        $confcorreo = $informacion->confcorreo;
        $password = $informacion->password;
        $confpassword = $informacion->confpassword;
        //Se valida que el correo sea el mismo (confirmacion)
        if(($correo != $confcorreo)){
            return view("registroAdmin",["estatus"=> "error", "mensaje"=> "Los Correos No Coinciden"]);
        }
        //Se valida que la contraseña sea el mismo (confirmacion)
        if(($password != $confpassword)){
            return view("registroAdmin",["estatus"=> "error", "mensaje"=> "Las Contraseñas no Coinciden"]);
        }
        //Se sube la informacion a DB
        $admin = new Admin();
        $admin->nombre =  $nombre;
        $admin->aPaterno = $aPaterno;
        $admin->aMaterno = $aMaterno;
        $admin->cargo = $cargo;
        $admin->correo =  $correo;
        $admin->password = bcrypt($password);
        $admin->save();
        return view("loginAdmin",["estatus"=> "aprobado", "mensaje"=> "¡Cuenta de Socio Creada!"]);
    }

    //Agregar Producto
    public function agregar(){
        return view('agregarProducto');
    }
    public function agregarProducto(Request $informacion){
        //Valida que se hayan llenado todos los datos
        if(!$informacion->nombre || !$informacion->precio || !$informacion->descripcion || !$informacion->categoria ||
            !$informacion->color || !$informacion->detalles || !$informacion->foto1 || !$informacion->foto2 ||
            !$informacion->foto3 || !$informacion->foto4 || !$informacion->foto5){
            return view("agregarProducto",["estatus"=> "error", "mensaje"=> "¡Complete todos los campos Obligatorios!"]);
        }
        //Se extraen todas las variables del formulario
        $nombre = $informacion->nombre;
        $precio = $informacion->precio;
        $descuento = $informacion->precioE;
        $descripcion = $informacion->descripcion;
        $categoria = $informacion->categoria;
        $color = $informacion->color;
        $detalles = $informacion->detalles;
        $foto1 = $informacion->foto1;
        $foto2 = $informacion->foto2;
        $foto3 = $informacion->foto3;
        $foto4 = $informacion->foto4;
        $foto5 = $informacion->foto5;

        //Se genera e nombre al azar
        $letras = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z');
        $letrasM = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z');
        $numeros = array('1', '2', '3', '4', '5', '6', '7', '8', '9');
        //FOTO
        $archivofoto1 = $informacion->file('foto1'); //La foto se almacena en una varaible
        $archivofoto2 = $informacion->file('foto2'); //La foto se almacena en una varaible
        $archivofoto3 = $informacion->file('foto3'); //La foto se almacena en una varaible
        $archivofoto4 = $informacion->file('foto4'); //La foto se almacena en una varaible
        $archivofoto5 = $informacion->file('foto5'); //La foto se almacena en una varaible
        //El nombre de la imagen constara de 30 caracteres, y se validara que no sea el uno ya registrado
        $nombreImagen = null;
        do{
            for($x = 0; $x<10; $x++){
                $nombreImagen = $nombreImagen.$letras[array_rand($letras)];//Asignamos una letra minuscula al azar
                $nombreImagen = $nombreImagen.$letrasM[array_rand($letrasM)];//Asignamos una letra mayuscula al azar
                $nombreImagen = $nombreImagen.$numeros[array_rand($numeros)];//Asignamos un numero al azar
            }
            $extensionfoto1 = $foto1->getClientOriginalExtension();//Extraemos la extension de la foto
            $extensionfoto2 = $foto2->getClientOriginalExtension();//Extraemos la extension de la foto
            $extensionfoto3 = $foto3->getClientOriginalExtension();//Extraemos la extension de la foto
            $extensionfoto4 = $foto4->getClientOriginalExtension();//Extraemos la extension de la foto
            $extensionfoto5 = $foto5->getClientOriginalExtension();//Extraemos la extension de la foto
            if($extensionfoto1 != "png" && $extensionfoto1 != "jpg"){//validamos que sea una foto y no otro archivo
                return view("agregarProducto",["estatus"=> "foto1", "mensaje"=> "La foto debe de ser jpg o png"]);
            }
            if($extensionfoto2 != "png" && $extensionfoto2 != "jpg"){//validamos que sea una foto y no otro archivo
                return view("agregarProducto",["estatus"=> "foto2", "mensaje"=> "La foto debe de ser jpg o png"]);
            }
            if($extensionfoto3 != "png" && $extensionfoto3 != "jpg"){//validamos que sea una foto y no otro archivo
                return view("agregarProducto",["estatus"=> "foto3", "mensaje"=> "La foto debe de ser jpg o png"]);
            }
            if($extensionfoto4 != "png" && $extensionfoto4 != "jpg"){//validamos que sea una foto y no otro archivo
                return view("agregarProducto",["estatus"=> "foto4", "mensaje"=> "La foto debe de ser jpg o png"]);
            }
            if($extensionfoto5 != "png" && $extensionfoto5 != "jpg"){//validamos que sea una foto y no otro archivo
                return view("agregarProducto",["estatus"=> "foto5", "mensaje"=> "La foto debe de ser jpg o png"]);
            }
            $ruta = $nombreImagen;
            $nombrefoto1 = $categoria."f1".$nombreImagen.".".$extensionfoto1;
            $nombrefoto2 = $categoria."f2".$nombreImagen.".".$extensionfoto2;
            $nombrefoto3 = $categoria."f3".$nombreImagen.".".$extensionfoto3;
            $nombrefoto4 = $categoria."f4".$nombreImagen.".".$extensionfoto4;
            $nombrefoto5 = $categoria."f5".$nombreImagen.".".$extensionfoto5;
        }while($ruta == Productos::where('ruta',$ruta)->first());
        if($categoria == "futbol"){
            $archivofoto1->move(public_path().'/img/futbol/', $nombrefoto1);
            $archivofoto2->move(public_path().'/img/futbol/', $nombrefoto2);
            $archivofoto3->move(public_path().'/img/futbol/', $nombrefoto3);
            $archivofoto4->move(public_path().'/img/futbol/', $nombrefoto4);
            $archivofoto5->move(public_path().'/img/futbol/', $nombrefoto5);
        }
        if($categoria == "correr"){
            $archivofoto1->move(public_path().'/img/correr/', $nombrefoto1);
            $archivofoto2->move(public_path().'/img/correr/', $nombrefoto2);
            $archivofoto3->move(public_path().'/img/correr/', $nombrefoto3);
            $archivofoto4->move(public_path().'/img/correr/', $nombrefoto4);
            $archivofoto5->move(public_path().'/img/correr/', $nombrefoto5);
        }
        if($categoria == "fitness"){
            $archivofoto1->move(public_path().'/img/fitness/', $nombrefoto1);
            $archivofoto2->move(public_path().'/img/fitness/', $nombrefoto2);
            $archivofoto3->move(public_path().'/img/fitness/', $nombrefoto3);
            $archivofoto4->move(public_path().'/img/fitness/', $nombrefoto4);
            $archivofoto5->move(public_path().'/img/fitness/', $nombrefoto5);
        }
        $aux = null;
        if($descuento == "0"){
            $precioE = "0";
        }else{
            $aux = $descuento * 0.01;
            $precioD = $precio * $aux;
            $precioP = $precio - $precioD;
            $precioE = (int)$precioP;
        }
        //Se sube la informacion a DB
        $producto = new Productos();
        $producto->nombre =  $nombre;
        $producto->precio = $precio;
        $producto->descuento = $descuento;
        $producto->precioEs = $precioE;
        $producto->descripcion = $descripcion;
        $producto->color = $color;
        $producto->categorias = $categoria;
        $producto->detalle = $detalles;
        $producto->ruta = $ruta;
        $producto->foto1 = $nombrefoto1;
        $producto->foto2 = $nombrefoto2;
        $producto->foto3 = $nombrefoto3;
        $producto->foto4 = $nombrefoto4;
        $producto->foto5 = $nombrefoto5;
        $producto->save();
        return view("agregarProducto",["estatus"=> "aprobado", "mensaje"=> "Producto Registrado Correctamente"]);
    }

    public function futbol(){
        $productos = Productos::where("categorias", "futbol")->get();
        $categoria = Productos::select("categorias")->where("categorias", "futbol")->first();
        if($categoria == null){
            return view('futbol', ["usuario"=> "Admin"],  compact('productos'));
        }else{

            return view('futbol', ["usuario"=> "admin"], compact('productos'), compact('categoria'));
        }
    }
    public function correr(){
        $productos = Productos::where("categorias", "correr")->get();
        $categoria = Productos::select("categorias")->where("categorias", "correr")->first();
        if($categoria == null){
            return view('correr', ["usuario"=> "Admin"], compact('productos'));
        }else{

            return view('correr', ["usuario"=> "Admin"], compact('productos'), compact('categoria'));
        }
    }
    public function fitness(){
        $productos = Productos::where("categorias", "fitness")->get();
        $categoria = Productos::select("categorias")->where("categorias", "fitness")->first();
        if($categoria == null){
            return view('fitness', ["usuario"=> "Admin"], compact('productos'));
        }else{

            return view('fitness', ["usuario"=> "Admin"], compact('productos'), compact('categoria'));
        }
    }
    public function verProductoFutBol($id){
        $producto = Productos::where("id", $id)->where("categorias", "futbol")->first();
        return view('verProducto', ["usuario"=> "Admin"] , compact('producto'));
    }
    public function pedidos(){
        return view('verPedidos', ["admin"=> "Admin"]);
    }
    public function verPedidos(Request $request){
        if($request->ajax()){
            $pedidos = Compras::select("usuario", "producto", "precio", "categorias", "cantidad", "total",
                "referencia")->get();
            return DataTables::of($pedidos)
                ->make(true);
        }
        return redirect()->route('ver.pedidos.tienda');
    }
}
