@extends('layout.main')
@section('titulo', )
@section('css')

@endsection
@section('contenido')
    <div id="wrapper">
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <div class="container-fluid">
                    <h3 class="text-dark mb-4">Empleados-Examen</h3>
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Informacion de Empleados Top 5 MAYOR puntaje</p>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive table mt-2" id="tabla" role="grid" aria-describedby="dataTable_info">
                                <table class="table my-0" id="btempleado">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>nombre</th>
                                        <th>apellido Paterno</th>
                                        <th>correo</th>
                                        <th>sexo</th>
                                        <th>edad</th>
                                        <th>Calificacion</th>



                                    </tr>
                                    </thead>
                                    <tbody id="tbempleados">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Informacion de Empleados Top 5 MENOR puntaje</p>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive table mt-2" id="tabla" role="grid" aria-describedby="dataTable_info">
                                <table class="table my-0" id="btempleado">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>nombre</th>
                                        <th>apellido Paterno</th>
                                        <th>correo</th>
                                        <th>sexo</th>
                                        <th>edad</th>
                                        <th>Calificacion</th>



                                    </tr>
                                    </thead>
                                    <tbody id="tbempleadosmin">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>


        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>

@endsection
@section('js')
    <script src="{{asset('/js/Empleado.js')}}"></script>
    <script>

    </script>
@endsection
