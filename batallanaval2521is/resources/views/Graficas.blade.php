@extends('layout.main')
@section('titulo', )
@section('css')

@endsection
@section('contenido')
    <div class="card shadow">
        <div class="card shadow">
            <br>
            <div class="row col-md-12">
                <div class="col-md-6">

                    <h4>Alumnos con mayor calificacion</h4>
                    <canvas id="myChart" width="400" height="300"></canvas>
                </div>
                <div class="col-md-6">
                    <h4>Alumnos con Menor Calificacion</h4>
                    <canvas id="myChart2" width="400" height="300"></canvas>
                </div>
            </div>
        </div>
        <br>
        <div class="row col-md-12">

            <h4>Calificaciones</h4>
            <canvas id="myChart5" width="400" height=200"></canvas>

        </div>
        <br>
            <div class="row col-md-12">
                <div class="col-md-5">
                    <h4>APromedio por Genero</h4>
                    <canvas id="myChart3" width="400" height="400"></canvas>
                </div>
                <div class="col-md-5">
                    <h4>Promedio por edad</h4>
                    <canvas id="myChart4" width="400" height="400"></canvas>
                </div>

            </div>

    </div>
        <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>


@endsection
@section('js')
    <script src="{{asset('/js/Empleado.js')}}"></script>
@endsection
