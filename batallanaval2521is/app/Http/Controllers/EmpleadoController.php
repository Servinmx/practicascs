<?php

namespace App\Http\Controllers;

use App\Models\Empleado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpleadoController extends Controller
{
    public function examen(){
        return view("examen");
    }
    public function Graficas(){
        return view("Graficas");
    }

    public function agregaRegistro(Request $request){


        $emp = new Empleado();
        $emp->nombre =  $request->nombre;
        $emp->apeidoP = $request->apeidoP;
        $emp->apeidoM =  $request->apeidoM;
        $emp->correo =  $request->correo;
        $emp->fecha =  $request->fecha;
        $emp->sexo =  $request->sexo;
        $emp->edad =  $request->edad;
        $emp->respuesta1 =  $request->respuesta1;
        $emp->respuesta2 =  $request->respuesta2;
        $emp->respuesta3 =  $request->respuesta3;
        $emp->respuesta4 =  $request->respuesta4;
        $emp->respuesta5 =  $request->respuesta5;
        $emp->respuesta6 =  $request->respuesta6;
        $emp->respuesta7 =  $request->respuesta7;
        $emp->respuesta8 =  $request->respuesta8;
        $emp->respuesta9 =  $request->respuesta9;
        $emp->respuesta10 =  $request->respuesta10;
        $respuestas = null;
        $respuestas =$request->respuesta1 + $request->respuesta2 +$request->respuesta3 +$request->respuesta4 +$request->respuesta5 +$request->respuesta6 +$request->respuesta7 +$request->respuesta8 +$request->respuesta9 +$request->respuesta10 ;
            $emp->calificacion = $respuestas;
        $emp->save();
        return view("menuExamen",["estatus"=> "success", "mensaje"=> "¡examen terminado!", "puntaje" => $respuestas]);
    }
    public function MostrarEmpleado(){
        return view("Empleados");
    }
    public function MostrarEmpleadotop(){
        return view("TopEmpleados");
    }
    public function encontrar()
    {
        $Empleado = Empleado::all();

        return response()->json($Empleado);
    }

    public function registrarCalificacion($idEmpleado, $calificacion)
    {
        echo json_encode($idEmpleado,$calificacion);


    }
    public function verificarCredenciales(Request $datos)
    {

        if (Empleado::where('correo', $datos->correo)->exists()) {
            return view("examen", ["estatus" => "error", "mensaje" => "¡Ya se realiazo examen con este correo"]);
        }
        return redirect()->route('examen');
    }

    public function MaximoPuntaje(){

        $Empleados=Empleado::orderBy("calificacion","DESC")->where("calificacion","<=",10)->get();
        return response()->json($Empleados);
    }

    public function MinimoPuntaje(){

        $Empleados=Empleado::orderBy("calificacion","ASC")->where("calificacion",">=",0)->get();
        return response()->json($Empleados);
    }
    public function empleadoi(){

        $Empleados=Empleado::orderBy("id","DESC")->get();
        return response()->json($Empleados);
    }
    function imrpimir(){

      $emp = Empleado::all() -> last();
        $pdf= \PDF::loadView('pdf',compact("emp"));
        return $pdf->download('calificacion.pdf');
    }
function informacion(){

}
}
