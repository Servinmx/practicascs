var nombresTotales =[];
var calificacionesTotales= [];
$(document).ready(function ()
{
    $.ajax({
        method: "get",
        url:"/Empleados/mostrar",
        success:function (response)
        {
            for(var i=0;i<response.length;i++) {
                $('#tbempleado').append('<tr><td>' + response[i].id + '</td><td>' + response[i].nombre + '</td><td>' + response[i].apeidoP+ '</td><td>' + response[i].correo + '</td><td>' + response[i].sexo + '</td><td>' + response[i].edad + '</td><td>' + response[i].calificacion + '</td></tr>');
                nombresTotales.push(response[i].nombre);
                calificacionesTotales.push(response[i].calificacion);
            }
            generarGrafica3()
            generarGrafica4()
            generarGrafica5(nombresTotales,calificacionesTotales)
        }
    })
    function generarGrafica3(){
        var ctx = document.getElementById('myChart3');

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['hombres','Mujeres'],
                datasets: [{
                    label: 'Calificaciones mas Bajas',
                    data: [8,6],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

    }
    function generarGrafica4(){
        var ctx = document.getElementById('myChart4');

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['años 21','25','28','35','42','50'],
                datasets: [{
                    label: 'Calificacion por edad',
                    data: [8,6,6,7,10,13],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

    }

    function generarGrafica5(nombresTotales,calificacionesTotales){
        var ctx = document.getElementById('myChart5');

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: nombresTotales,
                datasets: [{
                    label: 'Calificaciones mas Bajas',
                    data: calificacionesTotales,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

    }

})


var nombresTop =[];
var calificacionTop = [];
$(document).ready(function ()
{
    $.ajax({
        method: "get",
        url:"/Empleados/mostrartop",
        success:function (response)
        {
            for(var i=0;i<5;i++) {
                $('#tbempleados').append('<tr><td>' + response[i].id + '</td><td>' + response[i].nombre + '</td><td>' + response[i].apeidoP+ '</td><td>' + response[i].correo + '</td><td>' + response[i].sexo + '</td><td>' + response[i].edad + '</td><td>' + response[i].calificacion + '</td></tr>');
                nombresTop.push(response[i].nombre);
                calificacionTop.push(response[i].calificacion);
            }
            generarGrafica(nombresTop,calificacionTop);
        }
    })
    function generarGrafica(nombresMin, calificacionesMin){
        var ctx = document.getElementById('myChart');

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: nombresMin,
                datasets: [{
                    label: 'Calificaciones mas Altas',
                    data: calificacionesMin,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

    }

})
var nombresMin =[];
var calificacionesMin = [];
$(document).ready(function ()
{
    $.ajax({
        method: "get",
        url: "/Empleados/mostrarmin",
        success: function (response) {
            for (var i = 0; i < 5; i++) {
                $('#tbempleadosmin').append('<tr><td>' + response[i].id + '</td><td>' + response[i].nombre + '</td><td>' + response[i].apeidoP + '</td><td>' + response[i].correo + '</td><td>' + response[i].sexo + '</td><td>' + response[i].edad + '</td><td>' + response[i].calificacion + '</td></tr>');
                nombresMin.push(response[i].nombre);
                calificacionesMin.push(response[i].calificacion);
            }
            generarGrafica2(nombresMin, calificacionesMin);
        }
    })
})
function generarGrafica2(nombresMin, calificacionesMin){
    var ctx = document.getElementById('myChart2');

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: nombresMin,
            datasets: [{
                label: 'Calificaciones mas Bajas',
                data: calificacionesMin,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

}






