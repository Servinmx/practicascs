<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\TableroController;
use App\Http\Controllers\EmpleadoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('bienvenida');
});

Route::get('/bienvenida',[UsuarioController::class,'bienvenida'])->name('bienvenida');
Route::get('/login',[UsuarioController::class,'login'])->name('login');
Route::post('/login',[UsuarioController::class,'verificarCredenciales'])->name('login.form');
Route::get('/cerrarSesion',[UsuarioController::class,'cerrarSesion'])->name('cerrar.sesion');
Route::get('/registro',[UsuarioController::class,'registro'])->name('registro');
Route::post('/registro',[UsuarioController::class,'registroForm'])->name('registro.form');
Route::get('/examen',[EmpleadoController::class,'examen'])->name('examen');
Route::post('/registroEmpleado',[EmpleadoController::class,'agregaRegistro'])->name('registro.empleado');
Route::get('/imprimir',[EmpleadoController::class,'imrpimir'])->name('imprimir.examen');




Route::prefix('/Empleados')->middleware("VerificarUsuario")->group(function (){
    Route::get('/menu',[UsuarioController::class,'menu'])->name('usuario.menu');
    Route::get('/Empleado',[EmpleadoController::class,'MostrarEmpleado'])->name('mostrar.empleado');
    Route::get('/mostrar',[EmpleadoController::class,'encontrar'])->name('encontrar.empleado');
    Route::get('/mostrartop',[EmpleadoController::class,'MaximoPuntaje'])->name('encontrar.empleadotop');
    Route::get('/mostrarmin',[EmpleadoController::class,'MinimoPuntaje'])->name('encontrar.empleadomin');
    Route::get('/Empleadotop',[EmpleadoController::class,'MostrarEmpleadotop'])->name('mostrar.empleadotop');
  ;Route::get('/registrar/calificacion/examen/{idEmpleado?}/{calificacion?}',[EmpleadoController::class,'registrarCalificacion'])->name('registrar.calificacion.empleado');
    Route::get('/Graficas',[EmpleadoController::class,'Graficas'])->name('mostrar.Graficas');
});



